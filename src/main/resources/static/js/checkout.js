jQuery(document).ready(function () {
    "use strict";

    /**************************************************
     * AFFICHAGE STEPS
     **************************************************/

    $("#continue-step2").on("click", function () {
        //Fermer 1ère étape
        $("#opc-billing").removeClass("allow active");
        $("#checkout-step-billing").css("display", "none");
        //Ouvrir 2ème étape
        $("#opc-shipping").addClass("allow active");
        $("#checkout-step-shipping").css("display", "block");
    });

    $("#continue-step3").on("click", function () {
        //Fermer 2ème étape
        $("#opc-shipping").removeClass("allow active");
        $("#checkout-step-shipping").css("display", "none");
        //Ouvrir 3ème étape
        $("#opc-shipping_method").addClass("allow active");
        $("#checkout-step-shipping_method").css("display", "block");
    });

    $("#back-step1").on("click", function () {
        //Fermer 2ème étape
        $("#opc-shipping").removeClass("allow active");
        $("#checkout-step-shipping").css("display", "none");
        //Ouvrir 1ère étape
        $("#opc-billing").addClass("allow active");
        $("#checkout-step-billing").css("display", "block");
    });

    $("#continue-step4").on("click", function () {
        //Fermer 3ème étape
        $("#opc-shipping_method").removeClass("allow active");
        $("#checkout-step-shipping_method").css("display", "none");
        //Ouvrir 4ème étape
        $("#opc-payment").addClass("allow active");
        $("#checkout-step-payment").css("display", "block");
    });

    $("#back-step2").on("click", function () {
        //Fermer 3ème étape
        $("#opc-shipping_method").removeClass("allow active");
        $("#checkout-step-shipping_method").css("display", "none");
        //Ouvrir 2ème étape
        $("#opc-shipping").addClass("allow active");
        $("#checkout-step-shipping").css("display", "block");
    });

    $("#continue-step5").on("click", function () {
        //Fermer 4ème étape
        $("#opc-payment").removeClass("allow active");
        $("#checkout-step-payment").css("display", "none");
        //Ouvrir dernière étape
        $("#opc-review").addClass("allow active");
        $("#checkout-step-review").css("display", "block");
    });

    $("#back-step3").on("click", function () {
        //Fermer 4ème étape
        $("#opc-payment").removeClass("allow active");
        $("#checkout-step-payment").css("display", "none");
        //Ouvrir 3ème étape
        $("#opc-shipping_method").addClass("allow active");
        $("#checkout-step-shipping_method").css("display", "block");
    });

    $("#change-billing").on("click", function () {
        $("#opc-billing").addClass("allow active");
        $("#checkout-step-billing").css("display", "block");

        $("#opc-shipping").removeClass("allow active");
        $("#checkout-step-shipping").css("display", "none");

        $("#opc-shipping_method").removeClass("allow active");
        $("#checkout-step-shipping_method").css("display", "none");

        $("#opc-payment").removeClass("allow active");
        $("#checkout-step-payment").css("display", "none");

        $("#opc-review").removeClass("allow active");
        $("#checkout-step-review").css("display", "none");
    });

    $("#change-address").on("click", function () {
        $("#opc-billing").removeClass("allow active");
        $("#checkout-step-billing").css("display", "none");

        $("#opc-shipping").addClass("allow active");
        $("#checkout-step-shipping").css("display", "block");

        $("#opc-shipping_method").removeClass("allow active");
        $("#checkout-step-shipping_method").css("display", "none");

        $("#opc-payment").removeClass("allow active");
        $("#checkout-step-payment").css("display", "none");

        $("#opc-review").removeClass("allow active");
        $("#checkout-step-review").css("display", "none");
    });

    $("#change-shipping").on("click", function () {
        $("#opc-billing").removeClass("allow active");
        $("#checkout-step-billing").css("display", "none");

        $("#opc-shipping").removeClass("allow active");
        $("#checkout-step-shipping").css("display", "none");

        $("#opc-shipping_method").addClass("allow active");
        $("#checkout-step-shipping_method").css("display", "block");

        $("#opc-payment").removeClass("allow active");
        $("#checkout-step-payment").css("display", "none");

        $("#opc-review").removeClass("allow active");
        $("#checkout-step-review").css("display", "none");
    });

    /**************************************************
     * AFFICHAGE FORMULAIRE PAIEMENT
     **************************************************/
    $("#co-payment-form").change(function () {
        var selected_value = $("input[name='payment-mode']:checked").val();
        switch (selected_value) {
            case "paypal":
                $("#payment_form_paypal").css("display", "block");
                $("#payment_form_ccsave").css("display", "none");
                $("#recap-paiement-method").html("Paypal");
                break;
            case "creditcard" :
                $("#payment_form_ccsave").css("display", "block");
                $("#payment_form_paypal").css("display", "none");
                $("#recap-paiement-method").html("Carte de crédit");
                break;
            default :
                $("#payment_form_ccsave").css("display", "none");
                $("#payment_form_paypal").css("display", "none");
                $("#recap-paiement-method").html("Chèque");
        }
    });

    /**************************************************
     * PAIEMENT
     **************************************************/
    $("#payment-btn").on("click", function () {
        var error;
        var datas;
        var selected_value = $("input[name='payment-mode']:checked").val();
        switch (selected_value) {
            case "creditcard":
                if (!$("#ccsave_cc_owner").val() || !$("#ccsave_cc_number").val() || !
                        $("#ccsave_cc_cid").val() || $("#ccsave_cc_type").val() == "NO" ||
                    $("#ccsave_expiration").val() == "NO" ||
                    $("#ccsave_expiration_yr").val() == "NO") {
                    error = "Un ou plusieurs champs ne sont pas remplis."
                } else {
                    datas = {
                        "method": "creditcard",
                        "numeroCarte": $("#ccsave_cc_number").val(),
                        "cryptogramme": $("#ccsave_cc_cid").val(),
                        "dateExpiration": $("#ccsave_expiration").val() + "/" + $("#ccsave_expiration_yr").val()
                    };
                }
                break;
            case "paypal":
                if (!$("#paypal_email").val() || !$("#paypal_mdp").val()) {
                    error = "Un ou plusieurs champs ne sont pas remplis."
                } else {
                    datas = {"method": "paypal", "email": $("#paypal_email").val(), "password": $("#paypal_mdp").val()};
                }
                break;
            default:
                //Aucune vérification nécessaire
                datas = {"method": "cheque"};
        }
        if (error) {
            $("#error-msg").html(error);
        } else {
            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "/pay",
                data: JSON.stringify(datas),
                cache: false,
                timeout: 600000,
                success: function (data) {
                    $("#return-msg").html(data);
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                }
            });
        }
    });

});

