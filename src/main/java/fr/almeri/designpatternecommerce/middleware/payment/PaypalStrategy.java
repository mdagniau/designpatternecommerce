package fr.almeri.designpatternecommerce.middleware.payment;

public class PaypalStrategy implements PaiementStrategy {

    private String email;
    private String password;

    public PaypalStrategy(String email, String pass) {
        this.email = email;
        this.password = pass;
    }

    @Override
    public String payer(double montant) {
        return montant + "€ payés par Paypal.";
    }

}
