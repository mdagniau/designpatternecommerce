package fr.almeri.designpatternecommerce.middleware.payment;

public class CarteDeCreditStrategy implements PaiementStrategy {

    private String numeroCarte;
    private String cryptogramme;
    private String dateExpiration;

    /*TODO : faire des vérifs !!!*/
    public CarteDeCreditStrategy(String num, String crypto, String date) {
        this.numeroCarte = num;
        this.cryptogramme = crypto;
        this.dateExpiration = date;
    }

    @Override
    public String payer(double montant) {
        return montant + "€ payés par carte de crédit.";
    }

}
