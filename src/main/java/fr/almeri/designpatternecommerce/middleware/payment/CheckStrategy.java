package fr.almeri.designpatternecommerce.middleware.payment;

public class CheckStrategy implements PaiementStrategy {

    public CheckStrategy() {
    }

    @Override
    public String payer(double montant) {
        return montant + "€ payés par Chèque.";
    }

}
