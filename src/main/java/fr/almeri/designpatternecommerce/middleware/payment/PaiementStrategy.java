package fr.almeri.designpatternecommerce.middleware.payment;

public interface PaiementStrategy {
    public String payer(double montant);
}
