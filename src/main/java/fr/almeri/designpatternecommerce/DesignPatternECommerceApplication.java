package fr.almeri.designpatternecommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesignPatternECommerceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DesignPatternECommerceApplication.class, args);
    }

}
