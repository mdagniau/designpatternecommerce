package fr.almeri.designpatternecommerce.models.form;

import fr.almeri.designpatternecommerce.models.vehicules.Voiture;
import fr.almeri.designpatternecommerce.models.vehicules.VoitureAvecOptionDecorator;

import java.util.List;
import java.util.Locale;

public class VehiculeForm {

    private Voiture voiture;
    private String typeVoiture;
    private int qty;
    private List<VoitureAvecOptionDecorator> options;
    private String optionSelected;

    public VehiculeForm() {
    }

    public VehiculeForm(Voiture voiture) {
        this.voiture = voiture;
        this.typeVoiture = voiture.getNom().toUpperCase(Locale.ROOT);
        this.qty = 1;
        this.options = voiture.getOptionsDisponiblesDeBase();
    }

    public VehiculeForm(Voiture voiture, List<VoitureAvecOptionDecorator> options) {
        this.voiture = voiture;
        this.typeVoiture = voiture.getNom().toUpperCase(Locale.ROOT);
        this.qty = 1;
        this.options = options;
    }

    public Voiture getVoiture() {
        return voiture;
    }

    public void setVoiture(Voiture pVoiture) {
        this.voiture = pVoiture;
    }

    public String getTypeVoiture() {
        return typeVoiture;
    }

    public void setTypeVoiture(String pTypeVoiture) {
        this.typeVoiture = pTypeVoiture;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int pQty) {
        this.qty = pQty;
    }

    public List<VoitureAvecOptionDecorator> getOptions() {
        return options;
    }

    public void setOptions(List<VoitureAvecOptionDecorator> pOptions) {
        this.options = pOptions;
    }

    public String getOptionSelected() {
        return optionSelected;
    }

    public void setOptionSelected(String pOptionSelected) {
        this.optionSelected = pOptionSelected;
    }
}
