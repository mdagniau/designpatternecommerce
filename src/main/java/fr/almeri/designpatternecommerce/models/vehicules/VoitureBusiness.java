package fr.almeri.designpatternecommerce.models.vehicules;

import java.util.List;

public class VoitureBusiness extends VoitureAvecOptionDecorator {

    public VoitureBusiness(Voiture v) {
        setGamme("Voiture Business");
        setVehicule(v);
        List<String> options = this.getOptions();
        options.add("GPS");
        options.add("Radar de recul");
        this.setOptions(options);
    }

    public int getPrix() {
        return this.getVehicule().getPrix() + 1000;
    }

    public int getPoids() {
        return this.getVehicule().getPoids() + 20;
    }
}
