package fr.almeri.designpatternecommerce.models.vehicules;

public class DS extends Voiture {

    public DS() {
        this.setNom("DS");
        this.setMarque("Citroën");
        this.setCategorieFille("Citadine");
    }

    public int getPrix() {
        return 30000;
    }

    public int getPoids() {
        return 1500;
    }
}
