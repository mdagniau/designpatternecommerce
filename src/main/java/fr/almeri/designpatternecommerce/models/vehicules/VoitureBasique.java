package fr.almeri.designpatternecommerce.models.vehicules;

import java.util.List;

public class VoitureBasique extends VoitureAvecOptionDecorator {
    public VoitureBasique(Voiture v) {
        setGamme("Voiture Basique");
        setVehicule(v);

        List<String> options = this.getOptions();
        options.add("Aucune option");
        this.setOptions(options);
    }

    public int getPrix() {
        return this.getVehicule().getPrix();
    }

    public int getPoids() {
        return this.getVehicule().getPoids() + 20;
    }
}
