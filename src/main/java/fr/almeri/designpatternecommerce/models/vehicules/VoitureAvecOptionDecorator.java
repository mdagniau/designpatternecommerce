package fr.almeri.designpatternecommerce.models.vehicules;

public abstract class VoitureAvecOptionDecorator extends Voiture {

    private IVehicule vehicule;
    private String gamme;

    public String getGamme() {
        return gamme;
    }

    public void setGamme(String pGamme) {
        this.gamme = pGamme;
    }

    protected void setVehicule(IVehicule pVehicule) {
        setNom(pVehicule.getNom());
        setCategorieFille(pVehicule.getCategorieFille());
        setMarque(pVehicule.getMarque());
        setOptions(pVehicule.getOptions());
        this.vehicule = pVehicule;
    }

    public IVehicule getVehicule() {
        return this.vehicule;
    }
}
