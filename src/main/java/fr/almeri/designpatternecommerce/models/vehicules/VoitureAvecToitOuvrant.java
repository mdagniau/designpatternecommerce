package fr.almeri.designpatternecommerce.models.vehicules;

import java.util.List;

public class VoitureAvecToitOuvrant extends VoitureAvecOptionDecorator {

    public VoitureAvecToitOuvrant(VoitureAvecOptionDecorator pVoiture) {
        setGamme(pVoiture.getGamme() + " - Toit ouvrant");
        this.setVehicule(pVoiture);
        List<String> options = this.getOptions();
        options.add("Toit ouvrant");
        this.setOptions(options);
    }

    public int getPrix() {
        return this.getVehicule().getPrix() + 1000;
    }

    public int getPoids() {
        return this.getVehicule().getPoids() + 15;
    }
}
