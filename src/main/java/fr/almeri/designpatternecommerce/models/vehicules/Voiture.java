package fr.almeri.designpatternecommerce.models.vehicules;

import fr.almeri.designpatternecommerce.middleware.payment.PaiementStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class Voiture implements IVehicule {
    private final String categorie = "Voiture";
    private String categorieFille;

    private String nom;
    private String marque;

    private List<String> options = new ArrayList<>();
    private List<VoitureAvecOptionDecorator> optionsDisponiblesDeBase;
    private List<VoitureAvecOptionDecorator> optionsSupplementaires = new ArrayList<>();

    protected void setNom(String pNom) {
        this.nom = pNom;
    }

    public String getNom() {
        return this.nom;
    }

    protected void setMarque(String pMarque) {
        this.marque = pMarque;
    }

    public String getMarque() {
        return this.marque;
    }

    public String getCategorie() {
        return categorie;
    }

    public String getCategorieFille() {
        return categorieFille;
    }

    public void setCategorieFille(String pCategorieFille) {
        this.categorieFille = pCategorieFille;
    }

    public List<String> getOptions() {
        return this.options;
    }

    public void setOptions(List<String> pOptions) {
        this.options = pOptions;
    }

    public List<VoitureAvecOptionDecorator> getOptionsDisponiblesDeBase() {
        this.optionsDisponiblesDeBase = new ArrayList<>();
        optionsDisponiblesDeBase.add(new VoitureBasique(this));
        optionsDisponiblesDeBase.add(new VoitureBusiness(this));
        optionsDisponiblesDeBase.add(new VoitureLimited(this));
        return optionsDisponiblesDeBase;
    }

    public void setOptionsDisponiblesDeBase(List<VoitureAvecOptionDecorator> pOptionsDisponiblesDeBase) {
        this.optionsDisponiblesDeBase = pOptionsDisponiblesDeBase;
    }

    public List<VoitureAvecOptionDecorator> getOptionsSupplementaires() {
        return optionsSupplementaires;
    }

    public void setOptionsSupplementaires(List<VoitureAvecOptionDecorator> pOptionsSupplementaires) {
        this.optionsSupplementaires = pOptionsSupplementaires;
    }

    public String payer(PaiementStrategy pModePaiement) {
        double montant = this.getPrix();
        return pModePaiement.payer(montant);
    }

    @Override
    public String toString() {
        String desc = "{\"nom\":\"" + nom + "\", \"prix\":\"" + getPrix() + "€\", \"options\":\"";
        for (String option : options) {
            desc += option + "<br/>";
        }
      /* TODO
       if(optionsSupplementaires.size() > 0) {
            desc += "\", \"optionsSupp\":\"";
            for (VoitureAvecOptionDecorator option : optionsSupplementaires) {
                desc += option + "<br/>";
            }
        }*/
        desc += "\"}";
        return desc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Voiture voiture = (Voiture) o;
        return Objects.equals(categorie, voiture.categorie) && Objects.equals(categorieFille, voiture.categorieFille) && Objects.equals(nom, voiture.nom) && Objects.equals(marque, voiture.marque) && Objects.equals(options, voiture.options) && Objects.equals(optionsDisponiblesDeBase, voiture.optionsDisponiblesDeBase);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categorie, categorieFille, nom, marque, options, optionsDisponiblesDeBase);
    }
}
