package fr.almeri.designpatternecommerce.models.vehicules;

import java.util.List;

public class VoitureLimited extends VoitureAvecOptionDecorator {

    public VoitureLimited(Voiture pVoiture) {
        setGamme("Voiture Limited");
        this.setVehicule(pVoiture);
        List<String> options = this.getOptions();
        options.add("Caméra de recul");
        options.add("Anti franchissement de ligne blanche");
        options.add("Park assist");
        this.setOptions(options);
    }

    public int getPrix() {
        return this.getVehicule().getPrix() + 10000;
    }

    public int getPoids() {
        return this.getVehicule().getPoids() + 15;
    }
}
