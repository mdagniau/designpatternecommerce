package fr.almeri.designpatternecommerce.models.vehicules;

public class Renegade extends Voiture {

    public Renegade() {
        this.setNom("Renegade");
        this.setMarque("JEEP");
        this.setCategorieFille("SUV");
    }

    @Override
    public int getPrix() {
        return 22000;
    }

    @Override
    public int getPoids() {
        return 3050;
    }
}
