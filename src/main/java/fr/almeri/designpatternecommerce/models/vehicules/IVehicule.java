package fr.almeri.designpatternecommerce.models.vehicules;

import java.util.List;

public interface IVehicule {
    public String getNom();

    public String getCategorieFille();

    public String getMarque();

    public List<String> getOptions();

    public int getPrix();

    public int getPoids();
}
