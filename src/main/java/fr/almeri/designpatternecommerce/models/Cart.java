package fr.almeri.designpatternecommerce.models;

import fr.almeri.designpatternecommerce.middleware.payment.PaiementStrategy;
import fr.almeri.designpatternecommerce.models.vehicules.VoitureAvecOptionDecorator;

import java.util.HashMap;
import java.util.Map;

public class Cart {

    private HashMap<VoitureAvecOptionDecorator, Integer> cartList;
    private double montant;

    public Cart(HashMap<VoitureAvecOptionDecorator, Integer> cartList) {
        this.cartList = cartList;
    }

    public HashMap<VoitureAvecOptionDecorator, Integer> getCartList() {
        return cartList;
    }

    public void setCartList(HashMap<VoitureAvecOptionDecorator, Integer> pCartList) {
        this.cartList = pCartList;
    }

    public double getMontant() {
        montant = 0.0;
        for (Map.Entry<VoitureAvecOptionDecorator, Integer> entry : cartList.entrySet()) {
            montant += entry.getValue() * entry.getKey().getPrix();
        }
        return montant;
    }

    public void setMontant(double pMontant) {
        this.montant = pMontant;
    }

    public String payer(PaiementStrategy pModePaiement) {
        double montant = getMontant();
        return pModePaiement.payer(montant);
    }
}
