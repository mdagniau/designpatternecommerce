package fr.almeri.designpatternecommerce.controllers;

import fr.almeri.designpatternecommerce.models.form.VehiculeForm;
import fr.almeri.designpatternecommerce.models.vehicules.Voiture;
import fr.almeri.designpatternecommerce.models.vehicules.VoitureAvecOptionDecorator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

@Controller
public class CartController {

    @GetMapping("/view-cart")
    public String viewCart(Model pModel) {
        return "checkout/shopping_cart";
    }

    @PostMapping("/add-to-cart")
    public String addToCart(Model pModel, VehiculeForm voitureForm, HttpSession session) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        HashMap<VoitureAvecOptionDecorator, Integer> cartList = (HashMap<VoitureAvecOptionDecorator, Integer>) session.getAttribute("cart");
        if (cartList == null) {
            cartList = new HashMap<>();
        }
        Voiture voiture = ProductController.getVoitureFromType(voitureForm.getTypeVoiture());
        VoitureAvecOptionDecorator voitureOpt = ProductController.getVoitureWithGamme(voiture, voitureForm.getOptionSelected());
        if (voitureOpt != null) {
            if (cartList.containsKey(voitureOpt)) {
                cartList.put(voitureOpt, cartList.get(voitureOpt) + voitureForm.getQty());
            } else {
                cartList.put(voitureOpt, voitureForm.getQty());
            }
        }
        session.setAttribute("cart", cartList);
        session.setAttribute("totalPriceCart", getTotalPrice(cartList));
        return "redirect:/view-cart";
    }

    private double getTotalPrice(HashMap<VoitureAvecOptionDecorator, Integer> cartList) {
        double price = 0.0;
        for (Map.Entry<VoitureAvecOptionDecorator, Integer> entry : cartList.entrySet()) {
            price += entry.getKey().getPrix() * entry.getValue();
        }
        return price;
    }
}
