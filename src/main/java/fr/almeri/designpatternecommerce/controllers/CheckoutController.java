package fr.almeri.designpatternecommerce.controllers;

import fr.almeri.designpatternecommerce.middleware.payment.CarteDeCreditStrategy;
import fr.almeri.designpatternecommerce.middleware.payment.CheckStrategy;
import fr.almeri.designpatternecommerce.middleware.payment.PaypalStrategy;
import fr.almeri.designpatternecommerce.models.Cart;
import fr.almeri.designpatternecommerce.models.vehicules.VoitureAvecOptionDecorator;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;

@Controller
public class CheckoutController {

    @GetMapping("/checkout")
    public String checkout() {
        return "checkout/checkout";
    }

    @PostMapping("/pay")
    public String pay(@RequestBody String datas, Model pModel, HttpSession pHttpSession) {
        try {
            JSONObject infosPaiement = new JSONObject(datas);
            payer(pModel, infosPaiement, (HashMap<VoitureAvecOptionDecorator, Integer>) pHttpSession.getAttribute("cart"));
        } catch (JSONException err) {
            System.out.println(err.toString());
        }
        if (pModel.containsAttribute("error")) {
            return "checkout/error_paiement";
        }
        return "checkout/thanks";
    }

    private static Model payer(Model pModel, JSONObject infos, HashMap<VoitureAvecOptionDecorator, Integer> cartList) throws JSONException {
        Cart cart = new Cart(cartList);
        String pMode = infos.getString("method");
        switch (pMode) {
            case "cheque":
                pModel.addAttribute("paiementStatut", cart.payer(new CheckStrategy()));
                break;
            case "creditcard":
                pModel.addAttribute("paiementStatut", cart.payer(new CarteDeCreditStrategy(infos.getString("numeroCarte"),
                        infos.getString("cryptogramme"),
                        infos.getString("dateExpiration"))));
                break;
            case "paypal":
                pModel.addAttribute("paiementStatut", cart.payer(new PaypalStrategy(infos.getString("email"), infos.getString("password"))));
                break;
            default:
                pModel.addAttribute("error", "Cette méthode de paiement n'est pas prise en compte.");
        }
        return pModel;
    }


}
