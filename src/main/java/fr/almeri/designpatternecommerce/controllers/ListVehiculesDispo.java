package fr.almeri.designpatternecommerce.controllers;

import fr.almeri.designpatternecommerce.models.vehicules.DS;
import fr.almeri.designpatternecommerce.models.vehicules.Renegade;

public enum ListVehiculesDispo {
    DS(DS.class), RENEGADE(Renegade.class);

    private final Class vehiculeClass;

    ListVehiculesDispo(Class pVehiculeClass) {
        this.vehiculeClass = pVehiculeClass;
    }

    public Class getVehiculeClass() {
        return this.vehiculeClass;
    }
}
