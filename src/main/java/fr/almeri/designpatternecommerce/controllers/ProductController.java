package fr.almeri.designpatternecommerce.controllers;

import fr.almeri.designpatternecommerce.models.form.VehiculeForm;
import fr.almeri.designpatternecommerce.models.vehicules.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

@Controller
public class ProductController {
    @GetMapping("/product/{type}")
    public String getDetailProduct(@PathVariable(value = "type") String type, Model pModel) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Voiture voiture = getVoitureFromType(type);

        if (voiture != null) {
            pModel.addAttribute("category", voiture.getCategorie());
            pModel.addAttribute("childCategory", voiture.getCategorieFille());
            pModel.addAttribute("voitureForm", new VehiculeForm(voiture));
        } else {
            //If case is not used
            return "redirect:/404";
        }

        return "product_detail";
    }

    @PostMapping("/product/{type}/{gamme}")
    public ResponseEntity<String> getOptionsFromGamme(@PathVariable(value = "type") String type, @PathVariable(value = "gamme") String gamme, Model pModel) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Voiture voiture = getVoitureFromType(type);
        VoitureAvecOptionDecorator voitureOpt = getVoitureWithGamme(voiture, gamme);
     /* TODO
     if("Voiture Limited".equals(gamme)){
            voitureOpt.getOptionsSupplementaires().add(new VoitureAvecToitOuvrant(voitureOpt));
            voitureOpt.setOptionsSupplementaires(voitureOpt.getOptionsSupplementaires());
        }*/
        return ResponseEntity.ok(voitureOpt.toString());
    }

    /**
     * FACTORY ++ : design pattern Factory à l'aide de l'enum ListVehiculesDispo
     *
     * @param type
     * @return
     */
    public static Voiture getVoitureFromType(String type) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Constructor constructor = null;
        ListVehiculesDispo typeProduit = null;
        try {
            //Test if vehicule exists
            typeProduit = ListVehiculesDispo.valueOf(type.toUpperCase());
        } catch (IllegalArgumentException e) {
            //Else return null for generate error 404
            return null;
        }
        //Get constructor of class defined in enum
        constructor = typeProduit.getVehiculeClass().getConstructor();
        //Return instance of class
        return (Voiture) constructor.newInstance();
    }

    public static VoitureAvecOptionDecorator getVoitureWithGamme(Voiture voiture, String gamme) {
        switch (gamme) {
            case "Voiture Business":
                voiture = new VoitureBusiness(voiture);
                break;
            case "Voiture Limited":
                voiture = new VoitureBusiness(voiture);
                voiture = new VoitureLimited(voiture);
                break;
            default:
                voiture = new VoitureBasique(voiture);
        }
        return (VoitureAvecOptionDecorator) voiture;
    }
}
