# Projet Design Patterns

## Table des matières

1. [Informations générales](#1-informations-générales)
2. [Factory](#2-factory)
3. [Decorator](#3-decorator)
4. [Strategy](#4-strategy)
5. [FAQs](#5-faqs)

***

### 1. Informations générales

Ce projet a été créé en <b>JAVA 11</b>, utilise le framework <b>Spring Boot</b> et le moteur de template <b>
Thymeleaf</b>. Ceci est une V1 qui LARGEEEEEMENT améliorable ! Elle sera améliorée au fil du temps.

Il n'y a pas de base de données. Avec Spring Boot, la connexion à la base de données est faite automatiquement, c'est le
framework qui gère (comme Dctrine). Seul un fichier de conf suffit et de ce fait, nous ne nous rendons pas compte
explicitement du singleton qui en découle.

Les templates se trouvent dans le répertoire ```src/main/resources/templates```. Les fichiers statiques tels que le CSS,
JS and co se trouvent dans le répertoire ```src/main/resources/static```. Les fichiers JAVA se trouvent
dans ```src/main/java``` et sont triés par types de fichiers :

- les <i>controllers</i>;
- les <i>middleware</i> (outils qui interagissent directement avec le back et le front notamment le paiement ou encore
  le stockage de fichiers),
- les <i>models</i>.

Pour démarrer le projet en ligne de commande, il faut avoir [Maven](https://maven.apache.org/download.cgi) sur son PC et
lancer la commande ```mvn spring-boot:run``` ou faire <b>Run</b> dans <b>IntelliJ</b> directement.

***

### 2. Factory

Par rapport à ce que nous avons vu en cours, j'ai fait une version "améliorée" du design Pattern Factory. Ce dernier me
permet d'afficher les pages concernant les voitures : DS & Renegade.

Pour cela, j'ai crée une énumération ```ListVehiculesDispo.java``` (liste qui contient un nombre défini d'objets et qu'
on ne peut modifier) qui me servira pour remplacer cet énorme ```switch case``` que l'on pourrait imaginer. Cette
énumération contient le nom de la voiture ainsi que la classe correspondant à cette voiture. Ainsi, dans le controller
se chargeant d'afficher la page ``` ProductController.java```, pas besoin de ```switch case```, je vérifie si le
véhicule est disponible dans la liste et j'affiche les propriétés de cette dernière grâce à la classe associée. Si le
véhicule n'est pas disponible, je renvoie une erreur 404.

***

### 3. Decorator

Pour définir le design pattern Decorator, j'ai rajouté des gammes à mes voitures : basiques, business et limited. Chaque
gamme a ses propres options et chaque gamme supérieure englobe les options des gammes inférieures. Pour cela,
côté ```product_detail.html```, j'ai créé un ```select```permettant de choisir soit la gamme basique, soit une gamme
supérieure. En fonction du choix effectué, une requête AJAX est envoyée (méthode ```getVoitureWithGamme()```
dans ```ProductController.java```) afin de récupérer les options associées à cette gamme ainsi que le prix options
comprises. Il pourrait y avoir quelque chose de plus concret avec l'ajout d'options en plus sur les options comprises de
base comme le toit ouvrant ou autre. Cela sera implémenté dans une V2.

***

### 4. Strategy

Pour simuler le design pattern Strategy, j'ai repris ce que nous avions vu en cours. Lorsque vous vous apprétez à
payer (faites Continue pour les premières étapes), vous pouvez choisir votre mode paiement. En fonction de celui-ci, la
classe associée au mode de paiement est appelé et le texte renvoyé est donc différent.

***

### 5. FAQs

Si vous avez des questions, n'hésitez pas à ouvrir un ticket à
l'adresse ```contact-project+mdagniau-designpatternecommerce-32996591-issue-@incoming.gitlab.com```. Je vous répondrai
avec plaisir et rajouterai cela à la FAQs ici présente.
